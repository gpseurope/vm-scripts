import socket
import subprocess
from pyroute2 import IPRoute
import re
import os
import tempfile
import time

glassFishPort="8080"
editor='nano'

def checkEntryCrontab(service):
    out = subprocess.check_output(["crontab", "-l"])
    cronList = []
    line = ""
    for i in out:
        if i == '\n':
            cronList.append(line)
            line = ""
        else:
            line = line + i
    with open("cron.txt", "w") as cronFile:
        for i in cronList:
            if not service in i:
                cronFile.write(i + "\n")
    
    subprocess.call(["crontab", "cron.txt"])
    subprocess.call(["rm", "cron.txt"])
  
def validateIP(ip):
    pattern = "[a-zA-Z0-9]+(?:\.[a-zA-Z0-9]+){3}:[0-9]+"
    return re.match(pattern, ip)

def getHourMin():
    hour = 00
    while True:
        try:
            hour = int(raw_input("Hour: "))
            break
        except ValueError:
            print("No valid number")
    while hour > 23 or hour < 0:
        print("hour must be between 0-23")
        while True:
            try:
                hour = int(raw_input("Hour: "))
                break
            except ValueError:
                print("No valid number")

    minutes = 00
    while True:
        try:
            minutes = int(raw_input("Minutes: "))
            break
        except ValueError:
            print("No valid number")
    while minutes > 59 or minutes < 0:
        print("minutes must be between 0-59")
        while True:
            try:
                minutes = int(raw_input("Minutes: "))
                break
            except ValueError:
                print("No valid number")

    return hour, minutes
subprocess.call(['clear'])
print ("###################### Configuration Wizard #######################")
#print ("After you finish all the steps, you will be asked to confirm all the changes!\n")

ip = IPRoute()

#thisip

info = [{'iface': x['index'], 'addr': x.get_attr('IFA_ADDRESS'), 'mask':  x['prefixlen']} for x in ip.get_addr()]

machineIP = info[2].get("addr")

glassfishPATH_deploy = "/home/vagrant/epos/glassfish5/glassfish/domains/domain1/autodeploy"
print("Current Machine IP address is {0}. To change, power down VM. Next update the Vagrant file on the Host Machine. Then restart the VM(Can be used the command vagrant reload --provision!) .\n".format(machineIP))

print("\n###----------###")
print("DB Configuration\n")
try:
    resp = subprocess.check_output("sudo -u postgres psql -c \"SELECT 1 as res FROM pg_database where datname = \'gnss-europe\'\"", shell=True)
    if resp[15] == "1":
        print "DB already installed \n"
    elif resp[13] == "0":
        print("Installing DataBase...\n")
        time.sleep(1)
        subprocess.call("sudo su - postgres -c \"psql < /home/vagrant/epos/database/database/gnss-europe.pgdump.sql\"", shell=True)
        resp2 = subprocess.check_output("sudo -u postgres psql -c \"SELECT 1 as res FROM pg_database where datname = \'gnss-europe\'\"", shell=True)
        if resp2[15] == "1":
            print "\n DB installed sucessfully \n"
            raw_input("Press Enter to continue...")
        elif resp2[13] == "0":
            print("Installation error\n Exiting....")
            exit(1)          
except subprocess.CalledProcessError as err:
    print err


rT1 = ""
while rT1 != "y" and rT1 != "n":
    rT1 = raw_input(">Do you wish to install T1 triggers? (y/n)")
rT2 = ""
while rT2 != "y" and rT2 != "n":
    rT2 = raw_input(">Do you wish to install T2 triggers? (y/n)")

if rT1 == "y":
    if rT2 == "y":
        subprocess.call("/opt/epos/resetdatabase.tcsh T1 T2", shell=True)
        print "T1 and T2 triggers installed sucessfully"
    elif rT2 == "n":
        subprocess.call("/opt/epos/resetdatabase.tcsh T1", shell=True)
        print "T1 triggers installed sucessfully"
elif rT1 == "n" and rT2 == "y":
    subprocess.call("/opt/epos/resetdatabase.tcsh T2", shell=True)
    print "T2 triggers installed sucessfully"
# else:
#     subprocess.call("/opt/epos/resetdatabase.tcsh", shell=True)
#     print "T1 and T2 triggers installed sucessfully"
    

print("\n###----------###")
print("Current Database Properties in GlassServer:\n")
glassDBpropPATH = "/home/vagrant/epos/glassfish5/glassfish/domains/domain1/config/GLASS.conf"
pr = subprocess.Popen(['cat', glassDBpropPATH])
pr.wait()
print "-----"
val = ""
while (val != "y" and val != "n"):
    val = raw_input("\nThis file contains information of where the DB is located and also the email config about the timeseries. This file is read by GlassFramework.\n> Change file GLASS.conf?(y or n)?")
if val == "y":
    pr = subprocess.Popen([ editor , glassDBpropPATH])
    pr.wait()

# update DataGateway, GlassFramework and NodeManager

dataG_PATH = "/home/vagrant/epos/GLASS-web-client"
with open(dataG_PATH + "/app/config.js", "r") as fRead, open( dataG_PATH + "/app/temp.js", "w") as fWrite:
    for line in fRead: 
        if "server" in line and len(line.split("//")) == 2:
            varWrite = "  server:" + "\"http://" + machineIP +  ":"+glassFishPort+"/\"" + ",\n"
            fWrite.write(varWrite)
        else:
            fWrite.write(line)

subprocess.call("cp " + dataG_PATH + "/app/temp.js " + dataG_PATH + "/app/config.js" , shell=True)
pr = subprocess.call("rm " + dataG_PATH + "/app/temp.js", shell=True) 

nodeManager_PATH = "/home/vagrant/epos/NodeManager/src/environments"
with open(nodeManager_PATH + "/environment.prod.ts", "r") as fRead, open(nodeManager_PATH + "/temp.ts", "w") as fWrite:
    for line in fRead:
        if "baseURL" in line:
            varWrite = "  baseURL : " + "'" "http://" + machineIP + ":8080/GlassFramework/webresources/t0-manager/""'" + "\n"
            fWrite.write(varWrite)
        else:
            fWrite.write(line)  

print("\n###----------###")
print("\n NodeManager \n")
print("NodeManager purpose, is o improve the interaction between the user and the management of T0")
print("Building NodeManager...")
subprocess.Popen(['cp', nodeManager_PATH + "/temp.ts", nodeManager_PATH + "/environment.prod.ts" ])
os.chdir("/home/vagrant/epos/NodeManager/")
subprocess.call("ng build --prod --build-optimizer true --aot true", shell=True)
subprocess.call("grunt war", shell=True)
subprocess.call("cp  prod/NodeManager.war " + glassfishPATH_deploy, shell=True)
subprocess.call("rm " + nodeManager_PATH + "/temp.ts", shell=True)   

print("NodeManager built succesfully and moved to GlassFish autodeploy")
raw_input("Press Enter to continue...")
time.sleep(1)
os.chdir("/home/vagrant/epos")

dataGateWayStatus="set (but not running) to run"
startGlass = False
statusDG = False
res = "nothing"
os.chdir("GLASS-web-client/app")
print("\n###----------###")
print("\n DataGateway \n")
print("Data Gateway is a graphical user interface for the Data and Metadata provided by GlassFramework")
while res != "y" and res != "n":
    res = raw_input("\n> Activate Data Gateway and start it when the machines starts?(y or n)")
    subprocess.call("jar uvf ../dist/glasswebui.war config.js", shell=True)
    if res == "y":
        subprocess.call("cp " + dataG_PATH + "/dist/glasswebui.war " + glassfishPATH_deploy, shell=True)
        print("\n DataGateway application moved to GlassFish autodeploy!")
        startGlass = True
        statusDG = True
os.chdir("/home/vagrant/epos")
raw_input("\n Press Enter to continue...")

print("\n###----------###")
print("\n GlassFramework \n")
print("GlassFramework serves as backend for all the applications used here!")
print("\n Building GlassFramework \n")
os.chdir("/home/vagrant/epos/EPOS_GLASS_Framework/GlassFramework")
subprocess.call(["mvn", "package"])
subprocess.call(["cp", "target/GlassFramework.war", glassfishPATH_deploy])

print "\n GlassFramework built and deployed!"

os.chdir("/home/vagrant/epos")
raw_input("\n Press Enter to continue...")


print("\n###----------###")
print("\n PRODUCTS PORTAL .... \n")
statusPP = False
time.sleep(1)
prodPortal_PATH = "/home/vagrant/epos/Products_Portal/resources/assets/js"
currGlassIP_PP = 0
newGlassIP_PP = 0
with open(prodPortal_PATH + "/app.js", "r") as fRead, open( prodPortal_PATH + "/temp.js", "w") as fWrite:
    for line in fRead:
        if "_GLASSSERVER_" in line:
            varWrite = "var _GLASSSERVER_ = " + "\"http://" + machineIP + "/GlassFramework/webresources\"" + ";"
            fWrite.write(varWrite)
            fWrite.write("\n")
        else:
            fWrite.write(line)

subprocess.Popen(['cp', prodPortal_PATH + "/temp.js", prodPortal_PATH + "/app.js" ])
os.chdir("/home/vagrant/epos/Products_Portal")
print "Compilation output of Products Portal will be showed bellow"
time.sleep(1)
subprocess.call("npm run dev ", shell=True)
subprocess.call("rm " + prodPortal_PATH + "/temp.js", shell=True)
os.chdir("/home/vagrant/epos")
resp = raw_input("\n> Launch products portal when VM starts?(y or n)")
while resp != "y" and resp != "n":
    resp = raw_input("\n> Launch products portal when VM starts?(y or n)")
if resp == "y":
    checkEntryCrontab("Products_Portal")
    subprocess.call("(crontab -l; \"@reboot echo \"php /opt/epos/Products_Portal/artisan serve --host " + machineIP  + " --port 8000\") | crontab -", shell=True)
    startGlass = True
    statusPP = True
else:
    checkEntryCrontab("Products_Portal")

print("\n\n###----------###\n DB-API - Flask Web Server \n")
print("This solution's purpose is to function as a prototyping/testing tool for GNSS services.\n")
print("Current DB-API - Flask Web Server configuration will be shown below:\n")
fwssCFG_PATH = "/home/vagrant/epos/fwss/web_server.cfg"
pr = subprocess.Popen(['cat', fwssCFG_PATH])
pr.wait()
print "-----"
val = raw_input("> Change DB-API web_server.cfg?(y or n)")
while (val != "y" and val != "n"):
    val = raw_input("> Change DB-API web_server.cfg?(y or n)")
if val == "y":
    pr = subprocess.Popen([ editor , fwssCFG_PATH])
    pr.wait()



print("\n\n###----------###\n")
print("\n indexGD & DB-API \n")
print("\nindexGD is a python program to index geodetic data by the means of scanning through a folder with data, parsing information from data file names and sending the result, in the form of a JSON object to a specific web services, FWSS")

yn = raw_input("\n > Add indexGD and DB-API - Flask Web Server to crontab now?(y or n)")
while yn != "y" and yn != "n":
    yn = raw_input("> Add indexGD and DB-API - Flask Web Server to crontab now?(y or n)")

if( yn == "y"):
    print("When should indexGD and DB-API - Flask Web Server run?")
    hour, minutes = getHourMin()
    try:
        #retVal = subprocess.check_output("pgrep -f  web_server.py", shell=True, stderr=subprocess.STDOUT)
        minutesKill = 0
        hourKill = 0
        if minutes - 2 < 0:
            val = minutes - 2
            minutesKill = 60 + val
            if hour - 1 < 0:
                hourKill = 23
        else:
            minutesKill = minutes - 2
            hourKill = hour


        minutesRun = 0
        hourRun = 0
        if minutes - 1 < 0:
            val = minutes - 1
            minutesRun = 60 + val
            if hour - 1 < 0:
                hourRun = 23
        else:
            minutesRun = minutes - 1
            hourRun = hour
        
        re = ""
        while re != "n" and re != "y":
            re = raw_input("\n> indexGD must run with mandatory parameters. Open indexGD to check those and other optional parameters?(y or n)")
        path_indexGD = "/home/vagrant/epos/indexGD/"
        if re == "y":
            pr = subprocess.Popen(['cat', path_indexGD + "README.md"])
            pr.wait()
            print "-----"
        options = raw_input("> Add the desired parameters for indexGD:\n")
        checkEntryCrontab("web_server.py")
        checkEntryCrontab("indexGeodeticData.py")
        subprocess.call("(crontab -l; echo \"" + str(minutesKill) + " " + str(hourKill) + " * * * pkill -f web_server.py\") | crontab -", shell=True)
        subprocess.call("(crontab -l; echo \"" + str(minutesRun) + " " + str(hourRun) + " * * * cd /home/vagrant/epos/fwss; python /home/vagrant/epos/fwss/web_server.py >> /home/vagrant/epos/fwss/log.txt 2>&1 \") | crontab -", shell=True)
        subprocess.call("(crontab -l; echo \"" + str(minutes) + " " + str(hour) + " * * * python " + path_indexGD + "indexGeodeticData.py " + str(options) +" >> " + path_indexGD +"log.txt 2>&1 \") | crontab -", shell=True)

    except subprocess.CalledProcessError as error:
        print("FWSS is not running")
        print(error)
elif yn == "n":
    checkEntryCrontab("web_server.py")
    checkEntryCrontab("indexGeodeticData.py")
    

print("\n\n###----------###")
print("\n Syncronization\n")
time.sleep(1)


print "The Synchronization service is based on the crontab. Which means that depending on your choice, it will run at a desired time or when the VM starts. \n"
print "Current crontab is...\n"
#subprocess.call("crontab -l", shell=True)

res = ""
while res != "y" and res != "n":
    res = raw_input("This step can be done later using the command: startsynchronization. \n> Do you want to activate the sync Service now? (y or n) ")

if res == "y":
    print "\nThe Synchronization service configuration is shown below\n\n"

    syncServicePATH = "/home/vagrant/epos/EPOS_Sync_System/Sync_Service/SyncService.cfg"
    spr = subprocess.Popen(['cat', syncServicePATH])
    spr.wait()
    print "-----"
    val = raw_input("\n\n\n> Change file SyncService.cfg?(y or n)")
    while (val != "y" and val != "n"):
        val = raw_input("\n> Change SyncService.cfg?(y or n)")
    if val == "y":
        pr = subprocess.Popen([ editor, syncServicePATH])
        pr.wait()
    # pr = subprocess.Popen([ editor, syncServicePATH])
    # pr.wait()
    directory="/home/vagrant/epos/EPOS_Sync_System"

    with open(syncServicePATH, "r") as fSyncSercive:
        i = 0
        valDaemon = "a"
        for line in fSyncSercive:
            i+=1
            if line[0] == "#":
                continue
            if "daemon" in line:
                valDaemon = line.split("=")[1].split("\n")[0]
            if "interval" in line:
                interval = line.split("=")[1].split("\n")[0]
                checkEntryCrontab("SyncService.jar")
                if (valDaemon == "false"):
                    subprocess.call("(crontab -l; echo \"" + str(interval) + " java -jar /home/vagrant/epos/EPOS_Sync_System/dist/v0.2/SyncService.jar > /home/vagrant/epos/EPOS_Sync_System/log.txt 2>&1 \") | crontab -", shell=True)
                elif (valDaemon == "true"):
                    subprocess.call("(crontab -l; echo \"@reboot java -jar /home/vagrant/epos/EPOS_Sync_System/dist/v0.2/SyncService.jar > /home/vagrant/epos/EPOS_Sync_System/log.txt 2>&1 \") | crontab -", shell=True)
elif resp == "n":
    out = subprocess.check_output("crontab -l", shell=True)
    cronList = []
    line = ""
    for i in out:
        if i == '\n':
            cronList.append(line)
            line = ""
        else:
            line = line + i
    with open("cron.txt", "w") as cronFile:
        for i in cronList:
            if  not "SyncService.jar" in i:
                cronFile.write(i + "\n")
    
    subprocess.call("crontab cron.txt", shell=True)
    subprocess.call("rm cron.txt", shell=True)


if startGlass:
    checkEntryCrontab("start-domain")
    subprocess.call("(crontab -l; echo \"@reboot /home/vagrant/epos/glassfish5/bin/asadmin start-domain \") | crontab -", shell=True)
    print("\nGlassfish entry page will be running on http://{0}\n".format(machineIP+":"+glassFishPort ))

if statusDG:
    print "\nDataGateway is "+ dataGateWayStatus+ " on http://{0}\n".format(machineIP +":"+glassFishPort+"/glasswebui")

if statusPP:
    print "\nProducts Portal is set to run, but not running on http://{0}\n".format(machineIP +":8000")


print("\n\n###----------###")
print("Data should be synced to the following folders")
print("   Power Spectral Density Plots -> /opt/EPOS/power-spectral-density/plots/[STATION]/psd_figures")
print("   RINEX -> /opt/EPOS/RINEX/")
print("   SINEX -> /opt/EPOS/SINEX/")

print("\n\n###----------###")
print("\nVM must be restarted to work properly\n")
print("For more commands execute the command morecommands!")

print("##########------- DONE -------#########")





                




