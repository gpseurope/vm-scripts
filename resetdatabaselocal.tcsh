#!/bin/tcsh

set T1 = false;
set T2 = false;

foreach arg ($argv)
	switch ($arg)
		case "T1": 
			set T1 = true;
			breaksw
		case "T2":
			set T2 = true;
			breaksw
		default
			echo "Invalid argument";
			breaksw
	endsw
end

echo "Do you really want to reset the databases ? (y=yes)" 
set yn = "$<"
switch ( $yn )
   case [Yy] : 
	echo "reseting....."
	breaksw
   default : 
        exit
endsw

echo "  => Drop Database"
 su - postgres -c 'psql -d template1 -c "drop database \"gnss-europe\""'
if ($status != 0) then
   echo "Drop DataBase Failed"
   echo "Do you want to continue ? (y=yes)"
   set yn = "$<" 
   switch ( $yn )
   case [Yy] :
        echo "reseting....."
        breaksw
   default : 
	exit
   endsw
endif


echo "  => Loads Database"
 su - postgres -c 'psql -d template1 -c "\i /opt/epos/gpseurope/database/database/gnss-europe.pgdump.sql"'

echo "  => Fixes DB for 9 char"
 su - postgres -c 'psql -d gnss-europe -c "\i /opt/epos/gpseurope/database/database/marker9char.sql"'
 su - postgres -c 'psql -d gnss-europe -c "\i /opt/epos/gpseurope/database/database/marker9char.sql"'

echo "  => Adds T0 Triggers"
 su - postgres -c 'psql -d "gnss-europe" -c "\i /opt/epos/gpseurope/EPOS_Sync_System/Sync_Triggers/compiled_trigger_t0.sql"'
 su - postgres -c 'psql -d "gnss-europe" -c "\i /opt/epos/gpseurope/EPOS_Sync_System/Sync_Triggers/compiled_trigger_t0.sql"'

if ($T1 == true) then
	echo "  => Adds T1 Triggers"
	 su - postgres -c 'psql -d "gnss-europe" -c "\i /opt/epos/gpseurope/EPOS_Sync_System/Sync_Triggers/compiled_sync_t1.sql"'
	 su - postgres -c 'psql -d "gnss-europe" -c "\i /opt/epos/gpseurope/EPOS_Sync_System/Sync_Triggers/compiled_sync_t1.sql"'
endif

if ($T2 == true) then
	echo "  => Adds T2 Triggers"
	 su - postgres -c 'psql -d "gnss-europe" -c "\i /opt/epos/gpseurope/EPOS_Sync_System/Sync_Triggers/compiled_sync_t2.sql"'
	 su - postgres -c 'psql -d "gnss-europe" -c "\i /opt/epos/gpseurope/EPOS_Sync_System/Sync_Triggers/compiled_sync_t2.sql"'
endif

exit


