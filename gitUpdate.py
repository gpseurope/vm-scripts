import os
import subprocess as sp
# ver se ainda n esta na dir
currDir = os.getcwd()
if not (len(currDir.split("/")) == 4 and currDir.split("/")[3] == "epos"): 
    os.chdir("/home/vagrant/epos")
listDir = os.listdir(".")
try:
    sp.call("git config --global credential.helper cache", shell=True)
    sp.call("git config --global credential.helper \"cache --timeout=300\"", shell=True)
except OSError as e:
    print("Error configuring git cache " + str(e) + "\n Canceling update")
    exit(1) 
for folder in listDir:
    if not (os.path.isfile(folder)):        
        print "\n> Updating " + folder
        os.chdir(folder)
        print(os.getcwd())
        if os.path.exists(".git"):
            sp.call(["git", "stash"])
            try:
                resp = sp.check_output(["git", "pull"])
                print resp
            except sp.CalledProcessError as e:
                print str(e)
                exit(1)
            os.chdir("../")
        else:
            print("This project doesnt have repo. Update it manualy")
            os.chdir("../")


print "> Clearing git cache\n"
sp.call("git credential-cache exit", shell=True)
print "> Update Done"