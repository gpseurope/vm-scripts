#/bin/bash
#Update ANUBIS to precompiled shared library version

cwd=$(pwd)
curl http://www.pecny.cz/sw/anubis/ | grep "anu.*lin-shared-64b"

echo "Insert Version to Fetch (2.2.4)"
read version

cd /tmp

wget http://www.pecny.cz/sw/anubis/anubis-${version}-lin-shared-64b

if [ $? -eq 0 ];
then
  chmod +x anubis-${version}-lin-shared-64b
  ./anubis-${version}-lin-shared-64b -V
  if [ $? -eq 0 ]; then 
    mv  anubis-${version}-lin-shared-64b /home/vagrant/epos/anubis/app/
    rm /home/vagrant/epos/anubis/app/anubis 
    ln  -s /home/vagrant/epos/anubis/app/anubis-${version}-lin-shared-64b /home/vagrant/epos/anubis/app/anubis
  else
    echo "Failed to execute anubis.No changes made"
  fi
else
   echo "WGET DownLoad Failed"
fi
cd $cwd
anubis -V
