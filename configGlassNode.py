import subprocess
from pyroute2 import IPRoute
import re
import os
import tempfile
import time
import sys

glassFishPort="8080"
editor='nano'

def code(code1):
    return "\x1b[%sm" % str(code1)

def checkEntryCrontab(service):
    out = subprocess.check_output(["crontab", "-l"])
    cronList = []
    line = ""
    for i in out:
        if i == '\n':
            cronList.append(line)
            line = ""
        else:
            line = line + str(i)
    with open("cron.txt", "w") as cronFile:
        for i in cronList:
            if not service in i:
                cronFile.write(i + "\n")
    
    subprocess.call(["crontab", "cron.txt"])
    subprocess.call(["rm", "cron.txt"])
  
def validateIP(ip):
    pattern = "[a-zA-Z0-9]+(?:\.[a-zA-Z0-9]+){3}:[0-9]+"
    return re.match(pattern, ip)

def getHourMin():
    hour = 00
    while True:
        try:
            hour = int(raw_input("Hour: "))
            break
        except ValueError:
            print("No valid number")
    while hour > 23 or hour < 0:
        print("hour must be between 0-23")
        while True:
            try:
                hour = int(raw_input("Hour: "))
                break
            except ValueError:
                print("No valid number")

    minutes = 00
    while True:
        try:
            minutes = int(raw_input("Minutes: "))
            break
        except ValueError:
            print("No valid number")
    while minutes > 59 or minutes < 0:
        print("minutes must be between 0-59")
        while True:
            try:
                minutes = int(raw_input("Minutes: "))
                break
            except ValueError:
                print("No valid number")

    return hour, minutes
subprocess.call(['clear'])
print ("###################### Configuration Wizard #######################")
#print ("After you finish all the steps, you will be asked to confirm all the changes!\n")

#ip = IPRoute()

#info = [{'iface': x['index'], 'addr': x.get_attr('IFA_ADDRESS'), 'mask':  x['prefixlen']} for x in ip.get_addr()]

#externalMachineIP = info[2].get("addr")
localMachineIP = "" 
if 'thisIPLocal' in os.environ:
    localMachineIP = os.environ.get("thisIPLocal")
else:
    print ("No env variable thisIPLocal set on .bashrc")
    exit(1)

externalMachineIP = "" 
if 'thisIPExternal' in os.environ:
    externalMachineIP = os.environ.get("thisIPExternal")
else:
    print ("No env variable thisIPExternal set on .bashrc")
    exit(1)

if 'glassExternalPort' in os.environ:
    glassFishPort = os.environ.get("glassExternalPort")
else:
    print ("No env variable glassExternalPort set on .bashrc")
    exit(1)


glassfishPATH_deploy = "/home/vagrant/epos/glassfish5/glassfish/domains/domain1/autodeploy"
print("Current local machine IP address is {0} and external machine IP address is {1}.".format(localMachineIP, externalMachineIP))
print("To change the IP, power down VM. Then update the Vagrant file on the Host Machine.")
print("Then restart the VM(use the command vagrant reload --provision) .\n")

if sys.version_info[0] < 3:
	raw_input("Press Enter to continue...")
else:
	input("Press Enter to continue...")


print("\n###----------###")
print("DB Configuration\n")
try:
    resp = subprocess.check_output("sudo -u postgres psql -d template1 -c  \"SELECT 1 as res FROM pg_database where datname = \'gnss-europe\'\"", shell=True, universal_newlines=True)
    if resp[15] == "1":
        print ("DB already installed \n")
    elif resp[13] == "0":
        print("Installing DataBase...\n")
        time.sleep(1)
        subprocess.call("sudo su - postgres -c \"psql -d template1 < /home/vagrant/epos/database/database/gnss-europe.pgdump.sql\"", shell=True)
        resp2 = subprocess.check_output("sudo -u postgres psql -d template1 -c \"SELECT 1 as res FROM pg_database where datname = \'gnss-europe\'\"", shell=True, universal_newlines=True)
        if resp2[15] == "1":
            print ("\n DB installed sucessfully \n")
        elif resp2[13] == "0":
            print("Installation error\n Exiting....")
            exit(1)          
except subprocess.CalledProcessError as err:
    print (err)
   

print("\n###----------###")
GLASS_CONFIG_PATH = "/home/vagrant/epos/glassfish5/glassfish/domains/domain1/config"
DB_NAME = "gnss-europe"

with open(GLASS_CONFIG_PATH + "/glassTEMP.conf", "w") as fWrite, open( GLASS_CONFIG_PATH + "/GLASS.conf", "r") as fRead:
    for line in fRead: 
        if "dbip" in line:
            varWrite = "dbip=" + externalMachineIP + "\n"
            fWrite.write(varWrite)
        else:
            fWrite.write(line)

subprocess.call("cp " + GLASS_CONFIG_PATH + "/glassTEMP.conf " + GLASS_CONFIG_PATH + "/GLASS.conf" , shell=True)
pr = subprocess.call("rm " + GLASS_CONFIG_PATH + "/glassTEMP.conf", shell=True)



# update DataGateway, GlassFramework and NodeManager

dataG_PATH = "/home/vagrant/epos/GLASS-web-client"
with open(dataG_PATH + "/app/config.js", "r") as fRead, open( dataG_PATH + "/app/temp.js", "w") as fWrite:
    for line in fRead: 
        if "server" in line and len(line.split("//")) == 2:
            varWrite = "  server:" + "\"http://" + externalMachineIP +  ":"+glassFishPort+"/\"" + ",\n"
            fWrite.write(varWrite)
        else:
            fWrite.write(line)

subprocess.call("cp " + dataG_PATH + "/app/temp.js " + dataG_PATH + "/app/config.js" , shell=True)
pr = subprocess.call("rm " + dataG_PATH + "/app/temp.js", shell=True) 

nodeManager_PATH = "/home/vagrant/epos/NodeManager"
print("\n###----------###")
print("\n NodeManager \n")
print("NodeManager purpose, to improve the interaction between the user and the management of T0")
print("Building NodeManager...")
os.chdir("/home/vagrant/epos")
#subprocess.Popen(['cp', nodeManager_PATH + "/temp.ts", nodeManager_PATH + "/environment.prod.ts" ])
with open(nodeManager_PATH + "/prod/config.json", "r") as fRead, open(nodeManager_PATH + "/prod/temp.json", "w") as fWrite:
    for line in fRead:
        if "baseURL" in line:
            varWrite = "  \"baseURL\": " + "\"http://" + externalMachineIP + ":" + glassFishPort + "/GlassFramework/webresources/t0-manager/\" \n"
            fWrite.write(varWrite)
        else:
            fWrite.write(line)  
subprocess.call("cp " + nodeManager_PATH + "/prod/temp.json " + nodeManager_PATH + "/prod/config.json" , shell=True)
os.chdir("NodeManager/prod")
subprocess.call("jar uvf NodeManager.war config.json", shell=True)
subprocess.call("cp NodeManager.war " + glassfishPATH_deploy, shell=True)
subprocess.call("rm temp.json", shell=True)   

print("{}NodeManager built succesfully and moved to GlassFish autodeploy!{}".format(code(32), code(0)))
#raw_input("Press Enter to continue...")
time.sleep(1)
os.chdir("/home/vagrant/epos")

dataGateWayStatus="set to run"
startGlass = False
statusDG = False
res = "nothing"
os.chdir("GLASS-web-client/app")
print("\n###----------###")
print("\n DataGateway \n")
print("Data Gateway is a graphical user interface for the Data and Metadata provided by GlassFramework")
#while res != "y" and res != "n":
    #res = raw_input("\n> Activate Data Gateway and start it when the machines starts?(y or n)")
subprocess.call("jar uvf ../dist/glasswebui.war config.js", shell=True)
    #if res == "y":
subprocess.call("cp " + dataG_PATH + "/dist/glasswebui.war " + glassfishPATH_deploy, shell=True)
print("\n{} DataGateway application moved to GlassFish autodeploy!{}".format(code(32), code(0)))
startGlass = True
statusDG = True
os.chdir("/home/vagrant/epos")
#raw_input("\n Press Enter to continue...")

print("\n###----------###")
print("\n GlassFramework \n")
print("GlassFramework serves as backend for all the applications used here!\n")


#if glass has already been deployed
#copy serviceagreement.html to /tmp/tmpfile
os.chdir(glassfishPATH_deploy)
filesDeployed = os.listdir(os.curdir)
deployed = False
if "GlassFramework.war" in filesDeployed:
    os.chdir("/home/vagrant/epos/glassfish5/glassfish/domains/domain1/applications/GlassFramework")
    subprocess.call("mkdir -p /tmp/tmpfile/ && cp serviceagreement.html /tmp/tmpfile/serviceagreement.html", shell=True)
    deployed = True

os.chdir("/home/vagrant/epos/EPOS_GLASS_Framework/")
subprocess.call(["cp", "dist/GlassFramework.war", glassfishPATH_deploy])
optionSA = 0
print("Service agreement option. Choose one of the following options")
while optionSA < 1 or optionSA > 3 :
    if deployed:
        print ("\t 1 -> Use specific Service Agreement \n \t 2 -> Use default Service Agreement \n \t 3 -> Use Service Agreement which was already deployed\n")
    else: 
        print ("\t 1 -> Use specific Service Agreement \n \t 2 -> Use default Service Agreement \n")
    optionSA = int(input())
if optionSA == 1:
    saPATH = input("Please introduce the path\n")
    subprocess.call("cp " + saPATH + " /home/vagrant/epos/glassfish5/glassfish/domains/domain1/applications/GlassFramework/serviceagreement.html", shell=True) 
elif optionSA == 3:
    subprocess.call("cp /tmp/tmpfile/serviceagreement.html /home/vagrant/epos/glassfish5/glassfish/domains/domain1/applications/GlassFramework/serviceagreement.html", shell=True)


print ("\n {} GlassFramework deployed! {}".format(code(32),code(0)))

os.chdir("/home/vagrant/epos")
#raw_input("\n Press Enter to continue...")


print("\n\n###----------###\n DB-API - Flask Web Server \n")
print("The fwss projects purpose is to function as a prototyping/testing tool for GNSS services.\n")
#print("Current DB-API - Flask Web Server configuration will be shown below:\n")
fwssCFG_PATH = "/home/vagrant/epos/fwss"
with open(fwssCFG_PATH + "/web_server.cfg", "r") as fRead, open( fwssCFG_PATH + "/temp.cfg", "w") as fWrite:
    for line in fRead:
        if "hostname" in line:
            varWrite = "hostname: " + externalMachineIP
            fWrite.write(varWrite)
            fWrite.write("\n")
        if "db_name" in line:
            varWrite = "db_name: " + DB_NAME
            fWrite.write(varWrite)
            fWrite.write("\n")
        else:
            fWrite.write(line)


#-----------------------Sync------------------------

print ("The Synchronization service can be run several ways " )
print ( "\t -> (1) Sporadically on demand\n" + 
        "\t -> (2) Based on the crontab\n" +
        "\t -> (3) As a linux service based on the systemd framework\n" )

print ("Option 1 - The user wil have to run the service on demand") 
print ("Option 2 - The crontab can be used to run the sysc service, " + 
"periodically or when the VM starts" )
print ("Option 3 - Systemctl may be used to introspect and control" + 
"(status, start, stop) the state of the service and to start (enable)" + 
"when the vm starts\n" )
print ("The default designated way to run the sync. service is (3) using" +
"the systemd framework and the command systemctl\n" )


if startGlass:
    checkEntryCrontab("start-domain")
    subprocess.call("(crontab -l; echo \"@reboot /home/vagrant/epos/glassfish5/bin/asadmin start-domain \") | crontab -", shell=True)
    print("\nGlassfish entry page will be running on http://{0}\n".format(externalMachineIP+":"+glassFishPort ))

if statusDG:
    print ("\nDataGateway is "+ dataGateWayStatus+ " on http://{0}\n"
        .format(externalMachineIP +":"+glassFishPort+"/glasswebui"))



#print("\n\n###----------###")
#print("Data should be synced to the following folders")
#print("   Power Spectral Density Plots -> /opt/EPOS/power-spectral-density/plots/[STATION]/psd_figures")
#print("   RINEX -> /opt/EPOS/RINEX/")
#print("   SINEX -> /opt/EPOS/SINEX/")

print("\n\n###----------###")
#print("\nVM must be restarted to work properly\n")
print("For more commands execute the command morecommands!")

print("Advice: Check the GLASS config file in Domain1 GlassFish . just type catdb")

print("##########------- DONE -------#########")
