#!/bin/bash

echo "##########################"
echo "#                        #"
echo "#   Starting EPOS Apps   #"
echo "#                        #"
echo "##########################"
echo ""
echo "EPOS Started at "`date`
echo ""
echo "> Network Interface"
echo "  => My IP: $THISIP"
echo "  => Updating DNS servers"
echo "  => Adding Google DNS server to resolv.conf"
echo "  => You should change this line manually if you do not want to add this DNS"
sudo su -c "echo 'nameserver 8.8.8.8' >> /etc/resolv.conf"
echo  ""
echo "> WARNING - Postgres SQL should have started automatically"
echo "  => Please mind the output of the following lines"
echo ""
sudo su - postgres -c "psql -d template1 -l"
echo ""
echo "> Starting Glassfish Server"
asadmin restart-domain
echo ""
sleep 5
echo ""
echo "> Starting DB-API - Flask Web Server - in the background"
old=`pwd`
cd /opt/epos/fwss
python web_server.py >> /home/vagrant/epos/fwss/log.txt 2>&1 &
cd $old
sleep 5
echo ""
echo "##########################"
echo ""
echo "> To access: using the defaults" 
echo "  => The Glassfish Server entry page, navigate to:" 
echo "     http://$THISIP:8080"
echo ""
echo "  => Flask server"
echo "     http://$THISIP:5555"
echo ""
echo "  => All the EPOS scripts & programs, navigate to folder:"
echo "     /opt/epos/"
echo ""
echo "All Done"
