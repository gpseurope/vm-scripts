import os
import subprocess

os.chdir("/opt/epos/logs")
listDir = os.listdir(".")
for file in listDir:
    if (os.path.isfile(file)):        
        subprocess.call("tail -n 10000 " + file + " >> temp.txt", shell=True)
        subprocess.call(["cp", "temp.txt", file])
        subprocess.call(["rm", "temp.txt"])        